import express from 'express';
import * as config from '../config.json';
import { YoutubeService } from '../services/youtube';

const router = express.Router();
const service = new YoutubeService();

/* GET home page. */
router.get('/', async (req, res) => {
  const countryParam = req.query.code;
  let country = '';

  if(countryParam) {
    const exist = config.countryList.find((c) => {
      return c.code === countryParam;
    });

    if(typeof exist !== 'undefined') country = exist;
  }

  const trends = await service.getTrendingVideos(country.code);
  const results = Promise.all(trends);

  results
    .then(videos => {
      res.render('youtube/index', {
        title: config.title,
        videos: videos,
        countryname: country.name || 'List of countrys',
        countrys: config.countryList
      });
    })
    .catch(error => {
      const err = new Error();
            err.status = 500;
            err.stack = 'error fetching videos';

      res.status(500).render('error', {
        message: 'Ups! have a problem',
        error: err
      });
    })
});

/* GET video  */
router.get('/:videoId', async (req, res) => {
  res.render('youtube/player', {
    title: config.title,
    countryname: 'List of countrys',
    countrys: config.countryList 
  });
});

module.exports = router;
